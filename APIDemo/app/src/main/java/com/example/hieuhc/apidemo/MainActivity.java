package com.example.hieuhc.apidemo;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ListView lvContact;
    ArrayList<Info> array;
    //ArrayAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lvContact =(ListView) findViewById(R.id.lvContact);
        array = new ArrayList<>();
        //adapter = new ArrayAdapter(MainActivity.this, android.R.layout.simple_list_item_1,array);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new doc_JSON().execute("https://jsonplaceholder.typicode.com/posts");
            }
        });




    }
    class doc_JSON extends AsyncTask<String, Integer, String>{

        @Override
        protected String doInBackground(String... params) {
            StringBuilder content = new StringBuilder();
            try    {

                URL url = new URL(params[0]);
                URLConnection urlConnection = url.openConnection();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                String line;
               while ((line = bufferedReader.readLine()) != null){
                    content.append(line + "\n");
                }
                bufferedReader.close();
            }
            catch(Exception e)    {
                e.printStackTrace();
            }
            return content.toString();
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                JSONArray arrayJs = new JSONArray(s);
                for (int i=0; i<arrayJs.length();i++){
                    JSONObject oJ= arrayJs.getJSONObject(i);
                    array.add(new Info(
                            oJ.getString("userId"),
                            oJ.getString("id"),
                            oJ.getString("title"),
                            oJ.getString("body")
                    ));
                }
                //Toast.makeText(MainActivity.this, s, Toast.LENGTH_SHORT).show();
                Adapter adapter = new Adapter(MainActivity.this, R.layout.layout_info, array);
                lvContact.setAdapter(adapter);
            } catch (JSONException e) {
                e.printStackTrace();

            }

        }
    }



        //RequestQueue reques= Volley.newRequestQueue(this);

        //String url = "https://jsonplaceholder.typicode.com/posts";

        //JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            //@Override
            //public void onResponse(JSONArray response) {
            //for (int i=0 ; i < response.length(); i++){
                //try {
                   // JSONObject jsOpject = response.getJSONObject(i);
                   // String userID = jsOpject.getString("userId");
                    //String id = jsOpject.getString("id");
                    //String title = jsOpject.getString("title");
                    //String body = jsOpject.getString("body");
                //} catch (JSONException e) {
                  //  e.printStackTrace();
                //}
            //}
            //Toast.makeText(MainActivity.this, response.toString(), Toast.LENGTH_LONG).show();
            //}
        //},
            //    new Response.ErrorListener() {
              //      @Override
                //    public void onErrorResponse(VolleyError error) {

//                    }
  //              }
    //    );
//
  //      reques.add(jsonArrayRequest);



}
