package com.example.hieuhc.apidemo;

/**
 * Created by hieu.hc on 11/5/18.
 */

public class Info {
    public String userId;
    public  String id;
    public String title;
    public String body;

    public Info() {
    }

    public Info(String userId, String id, String title, String body) {
        this.userId = userId;
        this.id = id;
        this.title = title;
        this.body = body;
    }



}
