package com.example.hieuhc.apidemo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by hieu.hc on 11/5/18.
 */

public class Adapter extends ArrayAdapter<Info>{

    public  Adapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);

    }

    public Adapter(Context context, int resource, List<Info> items) {
        super(context, resource, items);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            view =  inflater.inflate(R.layout.layout_info, null);
        }
        Info p = getItem(position);
        if (p != null) {
            TextView txt1= (TextView) view.findViewById(R.id.txtText1);
            txt1.setText(p.userId);
            TextView txt2= (TextView) view.findViewById(R.id.txtText2);
            txt2.setText(p.id);
            TextView txt3 = (TextView) view.findViewById(R.id.txtText3);
            txt3.setText(p.title);
            TextView txt4= (TextView) view.findViewById(R.id.txtText4);
            txt4.setText(p.body);


        }
        return view;
    }
}
